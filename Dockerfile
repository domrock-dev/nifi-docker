FROM apache/nifi:1.5.0

LABEL maintainer="Paulo Sigrist <paulo.sigrist@domrock.com.br>"

COPY start.sh /opt/nifi/scripts/start.sh
