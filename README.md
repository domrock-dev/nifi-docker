# Apache Nifi - Docker

This is an extension of the [Apache NiFi Docker](https://hub.docker.com/r/apache/nifi/) to add the ability to configure a cluster. It basically changes the `start.sh` script to replace some properties in `nifi.properties`.

The new environment properties available are:

```
nifi.cluster.is.node: NIFI_CLUSTER_IS_NODE default value to false
nifi.cluster.node.address: NIFI_CLUSTER_NODE_ADDRESS default value to hostname
nifi.zookeeper.connect.string: NIFI_CLUSTER_ZOOKEEPER default to empty
nifi.cluster.node.protocol.port: NIFI_CLUSTER_NODE_PORT default to 7777
```

To use in a `docker-compose.yml` file:

```
version: '3.2'

services:

  nifi:
    image: domrock/nifi:1.5.0
    ports:
      - 8080
    environment: 
      - NIFI_CLUSTER_IS_NODE=true
      - NIFI_CLUSTER_ZOOKEEPER=192.168.0.22:2181

```
